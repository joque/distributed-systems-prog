Assignment
================

* Course Title: __Distributed Systems and Applications__
* Course Code: **DSA621S**
* Assessment: Second Assignment
* Released on: 30/09/2022
* Due Date: 16/10/2022 (hard deadline)

# Problem

## Description

We wish to build a distributed application to order and deliver groceries in this project. The grocery company has several stores, each with a list of items in specific quantities.

A customer eager to order items from the application must first be registered and obtain a customer number, which will be used in all subsequent transactions. A customer checks the available items to order groceries and indicates the requested quantity for each item and a delivery address.  Note that when the requested quantity for an item cannot be satisfied from a single store, the company should organise the delivery from several stores. Also, when the total available quantity of an item is less than what is ordered by the customer, a notification is sent to the latter to either adjust the quantity, cancel the item or cancel the order altogether.

Furthermore, all inventory information about the stores and the orders and delivery confirmation should be stored in a MongoDB data store. As well communication between services will use **Kafka**. Finally, each service will be deployed in a Docker container.

Your task is to implement the services in the Ballerina programming language and deploy them in a Docker container. You will also set up a **Kafka** cluster as well as a MongoDB instance.


## Evaluation criteria

We will follow the criteria below to assess each submission:

* Setup of the Kafka cluster, including topic management. (15%)
* Setup of the MongoDB instance. (15%)
* Docker container configuration. (15%)
* Implementation of the services in the **Ballerina language**. (50%)
* Quality of design and implementation. (5%)


# Submission Instructions

* This assignment is to be completed in groups of *at most* two students each.
* The submission date is Sunday, October 16 2022, at midnight (hard deadline). Please note that *commits* after that deadline will not be accepted. Therefore, a submission will be assessed based on a repository's clone at the deadline. For submissions with commits after the deadline, there will be a deduction of 10% per week.
* Any group that fails to submit on time will be awarded the mark 0.
* Although this is a group project, each member will receive a mark that reflects his/her contribution to the project. Each group member will be requested to highlight his/her contribution during the project presentation. More particularly, if a student's username does not appear in the commit log of the group repository, that student will be assumed not to have contributed to the project and thus be awarded the mark 0.
* Each group is expected to present its project after the submission deadline.
* There should be no assumption about the execution environment of your code. It could be run using a specific framework or on the command line.
* In the case of plagiarism (groups copying from each other or submissions copied from the Internet), all submissions involved will be awarded the mark 0, and each student will receive a warning.
